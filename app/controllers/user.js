
var User        = require('../../app/models/user'); // get our mongoose model
var jwt        = require('jsonwebtoken');
var config     = require('../../config');

var superSecret = config.secret;

module.exports = {
    get_users : function(req, res, next) {

        //GET URL atributes
        //console.log(req.params.id);
        User.find({}, function(err, users) {
            res.json(users);
        });

    },    
    authenticate : function(req, res) {

        // find the user
        User.findOne({
            email: req.body.email
        }).select('name email password').exec(function(err, user){

            if (err) throw err;

            if (!user) {
                res.json({ status: 'error', code: 100, message: 'Authentication failed. User not found.' });
            } else if (user) {

                // check if password matches
                if (!user.comparePassword(req.body.password)) {

                    res.json({ status: 'error', code: 101, message: 'Authentication failed. Wrong password.' });
                } else {

                    // if user is found and password is right
                    // create a token
                    var token = jwt.sign(user, superSecret, {
                        expiresInMinutes: 1051200 // expires in 2 years
                    });

                    res.json({
                        status: 'success',
                        message: 'Enjoy your token!',
                        data: { token: token }
                    });
                }       

            }

        });
    },
    createUser : function(req, res) {
        // check if user exists
        User.findOne({ 'email': req.body.email }, function(err, user) {

            // if there is no chris user, create one
            if (!user) {
                var NewUser = new User();

                NewUser.name        = req.body.name;  
                NewUser.email       = req.body.email; 
                NewUser.password    = req.body.password;
                NewUser.company     = req.body.company;
                NewUser.phone       = req.body.phone;

                NewUser.save();

                  NewUser.save(function(err,resp) {
                        if(err) {
                            console.log(err);
                            res.send({
                                status: 'error',
                                code: 10,
                                message :'Something went wrong',
                                missing: NewUser.validateSync().toString()
                            });
                        } else {
                            res.send({
                                status: 'success',
                                message:'User saved successfully',
                                data: null
                            });
                        }           

                    });

            } else {

                res.json({ status: 'error', code: 102, message: 'User Exists' });

            }

        });
    }

};