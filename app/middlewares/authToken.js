var jwt        = require('jsonwebtoken');
var config     = require('../../config');
var superSecret = config.secret;

module.exports = {

    authenticate : function(req, res, next) {
		// check header or url parameters or post parameters for token
		var token = req.body.token || req.param('token') || req.headers['x-access-token'];

		// decode token
		if (token) {

			// verifies secret and checks exp
			//jwt.verify(token, app.get('superSecret'), function(err, decoded) {	
			jwt.verify(token, superSecret, function(err, decoded) {			
				if (err) {
					return res.json({ status: 'error', message: 'Failed to authenticate token.', code: 51 });		
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;	
					next();
				}
			});

		} else {

			// if there is no token
			// return an error
			return res.status(403).send({ 
				status: 'error', 
				message: 'No token provided, not logged in',
				code: 50
			});
			
		}
    },
};