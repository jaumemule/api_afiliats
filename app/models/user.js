/*
// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('User', new Schema({ 
    name: String, 
    password: String, 
    admin: Boolean 
}));
*/
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var bcrypt 		 = require('bcrypt-nodejs');

// user schema 
var UserSchema   = new Schema({ 
	name: String,
	email: { type: String, required: true, index: { unique: true }},
	password: { type: String, required: true, select: false },
	company: { type: String, required: false, select: false },
	  phone: {
	    type: String,
	    validate: {
	      validator: function(v) {
	        return /d{3}-d{3}-d{4}/.test(v);
	      },
	      message: '{VALUE} is not a valid phone number!'
	    }
	  }
});

// hash the password before the user is saved
UserSchema.pre('save', function(next) {
	var user = this;

	// hash the password only if the password has been changed or user is new
	if (!user.isModified('password')) return next();

	// generate the hash
	bcrypt.hash(user.password, null, null, function(err, hash) {
		if (err) return next(err);

		// change the password to the hashed version
		user.password = hash;
		next();
	});
});

// method to compare a given password with the database hash
UserSchema.methods.comparePassword = function(password) {
	var user = this;
	return bcrypt.compareSync(password, user.password);
};

//module.exports = UserSchema;
module.exports = mongoose.model('User', UserSchema);

