
var config     = require('../config');
var jwt        = require('jsonwebtoken');

// super secret for creating tokens
var superSecret = config.secret;

module.exports = function(app, express) {

	var app = app;

	// ---------------------------------------------------------
	// MIDDLEWARES
	// ---------------------------------------------------------

	var authTokenMiddleware = require('../app/middlewares/authToken');

	// ---------------------------------------------------------
	// CONTROLLERS
	// ---------------------------------------------------------

	var UserController 	= require('../app/controllers/user');
	var IndexController = require('../app/controllers/index');

	// ---------------------------------------------------------
	// get an instance of the router for api routes
	// ---------------------------------------------------------
	var apiRoutes = express.Router(); 

	// ---------------------------------------------------------
	// authentication (no middleware necessary since this isnt authenticated)
	// ---------------------------------------------------------

	apiRoutes.post('/authenticate', UserController.authenticate);

	// ---------------------------------------------------------
	// Global middlewares examples (by position in document)
	// ---------------------------------------------------------
	//	app.use( apiRoutes, authTokenMiddleware );
	//	apiRoutes.use(function(req, res, next) {

	// ---------------------------------------------------------
	// authenticated routes
	// ---------------------------------------------------------

	apiRoutes.get('/', IndexController.construct);
	apiRoutes.get('/users',authTokenMiddleware.authenticate, UserController.get_users);
	apiRoutes.post('/users', UserController.createUser);

	apiRoutes.get('/check', function(req, res) {
		res.json(req.decoded);
	});

	return apiRoutes;
};