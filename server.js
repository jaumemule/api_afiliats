// =================================================================
// get the packages we need ========================================
// =================================================================
var express 	= require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var path    = require("path");
//var expressControllers = require('express-controller');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file
var User   = require('./app/models/user'); // get our mongoose model

// =================================================================
// configuration ===================================================
// =================================================================
var port = config.port; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

// =================================================================
// routes ==========================================================
// =================================================================

/*
// basic route (http://localhost:8080)
app.get('/', function(req, res) {
	res.send('Yeah! API Corrent per http://domain:' + port + '/api');
});
*/

// USE static files routing
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
  //res.sendFile('/public/index.html');
  res.sendfile(path.join(__dirname + 'index.html'));
});


// API ROUTES ------------------------
var apiRoutes = require('./app/routes')(app, express);
app.use('/api', apiRoutes);


// =================================================================
// start the server ================================================
// =================================================================
app.listen(port);
console.log('La magia corre a http://localhost:' + port);